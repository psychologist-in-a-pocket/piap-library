package de.rwth.comsys.piap.extensions.eval;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import de.rwth.comsys.piap.extensions.PiaPAbstractReceiver;

public abstract class StartEvalReceiver extends PiaPAbstractReceiver {

	private final String LOCK_KEY = "lock";

	@Override
	public void onReceive(Context context, Intent intent) {
		SharedPreferences prefs = context.getSharedPreferences(
				getPreferences(), Context.MODE_PRIVATE);
		boolean lock = prefs.getBoolean(LOCK_KEY, false);
		String action = intent.getAction();
		Intent myIntent = new Intent(context, getServiceName());
		Intent myActivity;

		if (action.equals(getAction() + ".START")) {
			Log.d("REC", "START SERVICE, REMOVE LOCK");
			prefs.edit().putBoolean(LOCK_KEY, false).apply();
			context.startService(myIntent);
			if (getActivityName() != null) {
				myActivity = new Intent(context, getActivityName());
				myActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(myActivity);
			}

		} else if (action.equals(getStartEvalAction())) {
			Log.d("REC", "START SERVICE");
			prefs.edit().putBoolean(LOCK_KEY, false).apply();
			context.startService(myIntent);

		}

		// if (intent.getAction().equals(
		// "android.intent.action.ACTION_POWER_CONNECTED")) {
		// Log.d("Receiver", "power connected");
		// inte.putExtra("Eval", true);
		// context.startService(inte);
		// }

	}

	protected String getStartEvalAction() {
		return "de.rwth.comsys.piap.startevalonly";
	}

}
