package de.rwth.comsys.piap.extensions.eval;


public abstract class PiaPOverallEvaluationExtension extends
		PiaPAbstractEvaluationExtension {

	/**
	 * The String representing a Piap Extension. Check for it with Intent
	 * filter.
	 */
	public static final String ACTION_PLUGIN = "de.rwth.comsys.piap.EvalOverall";

	/**
	 * Compute the depression score for a value
	 * 
	 * @param value
	 *            the value to compute the score for
	 * @return a score
	 */
	protected long computeScore(long value) {
		return 0;
	}
}
