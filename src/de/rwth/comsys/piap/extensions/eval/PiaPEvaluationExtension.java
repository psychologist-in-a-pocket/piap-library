package de.rwth.comsys.piap.extensions.eval;

import android.database.Cursor;
import de.rwth.comsys.piap.contentprovider.PluginCommunicationContract;
import de.rwth.comsys.piap.extensions.PiaPAbstractExtension;

public abstract class PiaPEvaluationExtension extends
		PiaPAbstractEvaluationExtension {
	public PiaPEvaluationExtension() {
	}

	/**
	 * The String representing a Piap Extension. Check for it with Intent
	 * filter.
	 */
	public static final String ACTION_PLUGIN = "de.rwth.comsys.piap.Eval";

	/**
	 * Returns the smallest timestamp of a specific datatype in the PluginDB
	 * 
	 * @param currentTime
	 *            The original minimum timestamp to start from
	 * @param dataType
	 *            The plugin datatype to search the pluginDataBase for
	 * @return
	 */
	protected long getMinumTimeStamp(long currentTime, String dataType) {
		String select = PluginCommunicationContract.C_MIMETYPE + " = ?";
		String[] selectArgs = { dataType };
		Cursor c = getContentResolver().query(
				PluginCommunicationContract.CONTENT_URI_PLUGINDATA,
				PluginCommunicationContract.COLUMNS_ALL, select, selectArgs,
				null);
		c.moveToFirst();
		long minumTime = currentTime;
		while (c.isAfterLast() == false) {
			long time = c.getLong(PiaPAbstractExtension.TIMESTAMP_COLUMN);
			if (time < minumTime) {
				minumTime = time;
			}
			c.moveToNext();
		}
		c.close();
		return minumTime;
	}

	/**
	 * Calculate the maximum data value of a day in the PluginDataDB of a
	 * specific datatype
	 * 
	 * @param dayBegin
	 *            The time where the day starts
	 * @param dayEnd
	 *            The time where the day ends
	 * @param dataType
	 *            The plugin datatype to search the pluginDataBase for
	 * @return A result object containing the timestamp and the value
	 */
	protected EvalNumericData calculateMaximumValueOfDay(long dayBegin,
			long dayEnd, String dataType) {
		String select = PluginCommunicationContract.C_MIMETYPE + " = ? AND "
				+ PluginCommunicationContract.C_TIMESTAMP + " > ? AND "
				+ PluginCommunicationContract.C_TIMESTAMP + " < ? ";
		String[] selectArgs = { dataType, "" + dayBegin, "" + dayEnd };
		long timestamp = 0;
		Cursor c = getContentResolver().query(
				PluginCommunicationContract.CONTENT_URI_PLUGINDATA,
				PluginCommunicationContract.COLUMNS_ALL, select, selectArgs,
				null);
		c.moveToFirst();

		c.moveToFirst();
		long maximumValueOfDay = 0;
		while (c.isAfterLast() == false) {
			long sleepTime = Long.parseLong(c
					.getString(PiaPAbstractExtension.DATA_COLUMN));
			if (sleepTime >= maximumValueOfDay) {
				maximumValueOfDay = sleepTime;
				timestamp = c.getLong(PiaPAbstractExtension.TIMESTAMP_COLUMN);
			}
			c.moveToNext();
		}
		EvalNumericData data = new EvalNumericData(timestamp, maximumValueOfDay);
		return data;
	}

	protected EvalNumericData calculateSumOfDay(long dayBegin, long dayEnd,
			String dataType) {
		String select = PluginCommunicationContract.C_MIMETYPE + " = ? AND "
				+ PluginCommunicationContract.C_TIMESTAMP + " > ? AND "
				+ PluginCommunicationContract.C_TIMESTAMP + " < ? ";
		String[] selectArgs = { dataType, "" + dayBegin, "" + dayEnd };
		long timestamp = 0;
		Cursor c = getContentResolver().query(
				PluginCommunicationContract.CONTENT_URI_PLUGINDATA,
				PluginCommunicationContract.COLUMNS_ALL, select, selectArgs,
				null);
		c.moveToFirst();

		long sum = 0;
		while (c.isAfterLast() == false) {
			long value = Long.parseLong(c
					.getString(PiaPAbstractExtension.DATA_COLUMN));
			sum += value;
			timestamp = c.getLong(PiaPAbstractExtension.TIMESTAMP_COLUMN);

			c.moveToNext();
		}
		EvalNumericData data = new EvalNumericData(timestamp, sum);
		return data;
	}

	/**
	 * Compute the depression score for a value
	 * 
	 * @param value
	 *            the value to compute the score for
	 * @return a score
	 */
	protected long computeScore(long value) {
		return 0;
	}

}
