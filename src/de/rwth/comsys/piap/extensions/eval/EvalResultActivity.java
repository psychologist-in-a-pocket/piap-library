package de.rwth.comsys.piap.extensions.eval;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import de.rwth.comsys.piap.contentprovider.EvaluationContract;
import de.rwth.comsys.piap.extensions.PiaPAbstractExtension;
import de.rwth.comsys.piap.lib.R;

public abstract class EvalResultActivity extends Activity {

	private GraphicalView evaluationChart;
	private XYMultipleSeriesDataset dataset;
	private XYMultipleSeriesRenderer renderer;
	private String mTitle = "Total";
	private int mColor = Color.RED;
	private Date[] mDates;
	private double[] mValues;
	public HashMap<Date, Integer> mMap = new HashMap<Date, Integer>(); // pair
	// of x
	// value and
	// all y
	// values
	public ArrayList<Date> mKeys = new ArrayList<Date>(); // x values
	private int mLimitTriggers = 0; // max y-Value
	private BroadcastReceiver receiver;
	private IntentFilter filter;
	private long maxYValue;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_charts);
		receiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.d("RECEIVER", "GOT INTENT");
				updateUI();
			}

		};
		filter = new IntentFilter();

		filter.addAction(PiaPAbstractEvaluationExtension.UPDATE_UI);
	}

	@Override
	protected void onStart() {
		super.onStart();
		registerReceiver(receiver, filter);
	}

	@Override
	protected void onStop() {
		super.onStop();
		unregisterReceiver(receiver);
	}

	@Override
	public void onResume() {
		super.onResume();
		SharedPreferences prefs = getSharedPreferences(
				PiaPAbstractEvaluationExtension.PREFERENCES_EVAL,
				Context.MODE_PRIVATE);
		prefs.edit()
				.putBoolean(
						PiaPAbstractEvaluationExtension.KEY_IS_ACTIVITY_ACTIVE,
						true).apply();
		updateUI();
	}

	@Override
	public void onPause() {
		super.onPause();
		SharedPreferences prefs = getSharedPreferences(
				PiaPAbstractEvaluationExtension.PREFERENCES_EVAL,
				Context.MODE_PRIVATE);
		prefs.edit()
				.putBoolean(
						PiaPAbstractEvaluationExtension.KEY_IS_ACTIVITY_ACTIVE,
						false).apply();
	}

	private void updateUI() {
		computeDailyData(mMap, mKeys, getDatatype());
		LinearLayout layout = (LinearLayout) findViewById(R.id.llChart);
		if (evaluationChart == null) {
			Log.d("CHART", "null");
			evaluationChart = ChartFactory.getTimeChartView(this, setupChart(),
					renderer, "dd.MM");
			layout.addView(evaluationChart);
		} else {
			Log.d("CHART", "not null");
			layout.removeView(evaluationChart);
			evaluationChart = ChartFactory.getTimeChartView(this, setupChart(),
					renderer, "dd.MM");
			layout.addView(evaluationChart);
		}

	}

	public void computeDailyData(HashMap<Date, Integer> map,
			ArrayList<Date> keys, String dataType) {
		// Data
		String select = EvaluationContract.C_MIMETYPE + " = ?";
		String[] selectArgs = { dataType };

		Cursor c = getContentResolver().query(
				EvaluationContract.CONTENT_URI_EVAL,
				EvaluationContract.COLUMNS_ALL, select, selectArgs, null);
		c.moveToFirst();
		long timestamp;
		long value;
		if (c.getCount() > 0) {
			maxYValue = Long.parseLong(c.getString(2));
		}
		while (!c.isAfterLast()) {
			timestamp = c.getLong(4);

			value = Long.parseLong(c.getString(2));
			Date d = getDate(timestamp);

			int resValue = 0;
			// update stats
			resValue += value;
			if (resValue > maxYValue) {
				maxYValue = resValue;
			}
			// add it
			map.put(d, resValue);
			keys.add(d);

			c.moveToNext();
		}
		c.close();

	}

	private XYMultipleSeriesDataset setupChart() {
		// fill it with the data
		// dates

		PointStyle[] styles = new PointStyle[1];
		styles[0] = PointStyle.CIRCLE;

		// PointStyle[] styles = new PointStyle[] { PointStyle.CIRCLE,
		// PointStyle.DIAMOND, PointStyle.TRIANGLE, PointStyle.SQUARE };
		renderer = buildRenderer(styles);
		int length = renderer.getSeriesRendererCount();
		for (int i = 0; i < length; i++) {
			((XYSeriesRenderer) renderer.getSeriesRendererAt(i))
					.setFillPoints(true);
			SimpleSeriesRenderer seriesRenderer = renderer
					.getSeriesRendererAt(i);
			seriesRenderer.setDisplayChartValues(true);
			seriesRenderer.setDisplayChartValuesDistance(0);
			// renderer.getSeriesRendererAt(i).setDisplayChartValues(true);
		}

		setChartSettings(renderer, "", "Date", getYCoordinateName(),
				System.currentTimeMillis() - PiaPAbstractExtension.DAY_IN_MS
						* 3, System.currentTimeMillis()
						+ PiaPAbstractExtension.DAY_IN_MS * 3, 0,
				maxYValue + 10, Color.BLACK, Color.BLACK);
		renderer.setXLabels(12);
		renderer.setYLabels(10);
		renderer.getSeriesRendererAt(0).setDisplayChartValues(false);
		renderer.setShowGrid(true);
		renderer.setXLabelsAlign(Align.RIGHT);
		renderer.setYLabelsAlign(Align.RIGHT);
		renderer.setZoomButtonsVisible(false);
		renderer.setXLabelsColor(Color.BLACK);
		renderer.setYLabelsColor(0, Color.BLACK);
		renderer.setBackgroundColor(Color.WHITE);
		renderer.setMarginsColor(Color.WHITE);

		// Log.i(TAG, "renderer.getSeriesRendererCount() " +
		// renderer.getSeriesRendererCount());
		// for (int i = 0; i < renderer.getSeriesRendererCount(); i++) {
		// renderer.getSeriesRendererAt(i).setDisplayChartValues(true);
		// }

		return buildDateDataset();
	}

	/**
	 * Overwrite this method to name your y coordinates
	 * 
	 * @return
	 */
	abstract protected String getYCoordinateName();

	abstract protected String getDatatype();

	protected XYMultipleSeriesDataset buildDateDataset() {
		mDates = new Date[mKeys.size()];
		for (int j = 0; j < mKeys.size(); j++) {
			mDates[j] = mKeys.get(j);

		}

		// alarmtriggers
		mValues = new double[mKeys.size()];
		for (int j = 0; j < mKeys.size(); j++) {
			int v = mMap.get(mKeys.get(j));
			mLimitTriggers = (v > mLimitTriggers ? v : mLimitTriggers);

			mValues[j] = v;

		}
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		TimeSeries series = new TimeSeries(mTitle);
		Date[] xV = mDates;
		double[] yV = mValues;
		int seriesLength = xV.length;
		for (int k = 0; k < seriesLength; k++) {
			series.add(xV[k], yV[k]);
			// Log.d("SETUP", "x: " + xV[k] + "y: " + yV[k]);

		}
		dataset.addSeries(series);

		return dataset;
	}

	/**
	 * Builds an XY multiple dataset using the provided values.
	 * 
	 * @param titles
	 *            the series titles
	 * @param xValues
	 *            the values for the X axis
	 * @param yValues
	 *            the values for the Y axis
	 * @return the XY multiple dataset
	 */
	protected XYMultipleSeriesDataset buildDataset(String[] titles,
			double[] xValues, double[] yValues) {

		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		addXYSeries(dataset, titles, xValues, yValues, 0);
		return dataset;
	}

	public void addXYSeries(XYMultipleSeriesDataset dataset, String[] titles,
			double[] xValues, double[] yValues, int scale) {
		int length = titles.length;
		for (int i = 0; i < length; i++) {
			XYSeries series = new XYSeries(titles[i], scale);
			double[] xV = xValues;
			double[] yV = yValues;
			int seriesLength = xV.length;
			for (int k = 0; k < seriesLength; k++) {
				series.add(xV[k], yV[k]);
			}
			dataset.addSeries(series);
		}
	}

	/**
	 * Sets a few of the series renderer settings.
	 * 
	 * @param renderer
	 *            the renderer to set the properties to
	 * @param title
	 *            the chart title
	 * @param xTitle
	 *            the title for the X axis
	 * @param yTitle
	 *            the title for the Y axis
	 * @param xMin
	 *            the minimum value on the X axis
	 * @param xMax
	 *            the maximum value on the X axis
	 * @param yMin
	 *            the minimum value on the Y axis
	 * @param yMax
	 *            the maximum value on the Y axis
	 * @param axesColor
	 *            the axes color
	 * @param labelsColor
	 *            the labels color
	 */
	protected void setChartSettings(XYMultipleSeriesRenderer renderer,
			String title, String xTitle, String yTitle, double xMin,
			double xMax, double yMin, double yMax, int axesColor,
			int labelsColor) {
		renderer.setChartTitle(title);
		renderer.setXTitle(xTitle);
		renderer.setYTitle(yTitle);
		renderer.setXAxisMin(xMin);
		renderer.setXAxisMax(xMax);
		renderer.setYAxisMin(yMin);
		renderer.setYAxisMax(yMax);
		renderer.setAxesColor(axesColor);
		renderer.setLabelsColor(labelsColor);
	}

	/**
	 * Builds an XY multiple series renderer.
	 * 
	 * @param colors
	 *            the series rendering colors
	 * @param styles
	 *            the series point styles
	 * @return the XY multiple series renderers
	 */
	protected XYMultipleSeriesRenderer buildRenderer(PointStyle[] styles) {
		XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
		int[] colorArr = { mColor };
		setRenderer(renderer, colorArr, styles);
		return renderer;
	}

	protected void setRenderer(XYMultipleSeriesRenderer renderer, int[] colors,
			PointStyle[] styles) {
		renderer.setAxisTitleTextSize(16);
		renderer.setChartTitleTextSize(20);
		renderer.setLabelsTextSize(15);
		renderer.setLegendTextSize(15);
		renderer.setPointSize(5f);
		renderer.setMargins(new int[] { 20, 30, 15, 20 });
		int length = styles.length;
		for (int i = 0; i < length; i++) {
			XYSeriesRenderer r = new XYSeriesRenderer();
			r.setColor(colors[i]);
			r.setPointStyle(styles[i]);
			renderer.addSeriesRenderer(r);
		}
	}

	/**
	 * Returns a Date initialized to the given year, month and day at midnight
	 * 
	 * @param timestamp
	 * @return
	 */
	public Date getDate(long timestamp) {
		Date d;
		Calendar c = new GregorianCalendar();
		c.setTimeInMillis(timestamp);
		c.set(Calendar.HOUR_OF_DAY, 24);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		d = (new Date(c.getTimeInMillis() - PiaPAbstractExtension.DAY_IN_MS));
		return d;
	}
}
