package de.rwth.comsys.piap.extensions.eval;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.filter.Approximator;
import com.github.mikephil.charting.data.filter.Approximator.ApproximatorType;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.Highlight;

import de.rwth.comsys.piap.contentprovider.EvaluationContract;
import de.rwth.comsys.piap.lib.R;

public abstract class EvalResultActivity2 extends Activity implements
		OnSeekBarChangeListener, OnChartValueSelectedListener {

	private LineChart mChart;
	public ArrayList<String> mKeys = new ArrayList<String>(); // x values
	private BroadcastReceiver receiver;
	private IntentFilter filter;
	private long maxYValue;
	public HashMap<String, Integer> mMap = new HashMap<String, Integer>(); // pair

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		receiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.d("RECEIVER", "GOT INTENT");
				updateUI();
			}

		};
		filter = new IntentFilter();

		filter.addAction(PiaPAbstractEvaluationExtension.UPDATE_UI);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_linechart);

		mChart = (LineChart) findViewById(R.id.chart1);
		mChart.setOnChartValueSelectedListener(this);

		mChart.setDrawGridBackground(false);

		// mChart.setStartAtZero(true);

		// enable value highlighting
		mChart.setHighlightEnabled(true);

		// enable touch gestures
		mChart.setTouchEnabled(true);

		// enable scaling and dragging
		mChart.setDragEnabled(true);
		mChart.setScaleEnabled(true);

		// if disabled, scaling can be done on x- and y-axis separately
		mChart.setPinchZoom(false);

	}

	@Override
	protected void onStart() {
		super.onStart();
		registerReceiver(receiver, filter);
	}

	@Override
	protected void onStop() {
		super.onStop();
		unregisterReceiver(receiver);
	}

	@Override
	public void onResume() {
		super.onResume();
		SharedPreferences prefs = getSharedPreferences(
				PiaPAbstractEvaluationExtension.PREFERENCES_EVAL,
				Context.MODE_PRIVATE);
		prefs.edit()
				.putBoolean(
						PiaPAbstractEvaluationExtension.KEY_IS_ACTIVITY_ACTIVE,
						true).apply();
		updateUI();
	}

	@Override
	public void onPause() {
		super.onPause();
		SharedPreferences prefs = getSharedPreferences(
				PiaPAbstractEvaluationExtension.PREFERENCES_EVAL,
				Context.MODE_PRIVATE);
		prefs.edit()
				.putBoolean(
						PiaPAbstractEvaluationExtension.KEY_IS_ACTIVITY_ACTIVE,
						false).apply();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.line, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int itemId = item.getItemId();
		if (itemId == R.id.actionToggleValues) {
			for (DataSet<?> set : mChart.getData().getDataSets())
				set.setDrawValues(!set.isDrawValuesEnabled());
			mChart.invalidate();
		} else if (itemId == R.id.actionTogglePinch) {
			if (mChart.isPinchZoomEnabled())
				mChart.setPinchZoom(false);
			else
				mChart.setPinchZoom(true);
			mChart.invalidate();
		} else if (itemId == R.id.actionToggleHighlight) {
			if (mChart.isHighlightEnabled())
				mChart.setHighlightEnabled(false);
			else
				mChart.setHighlightEnabled(true);
			mChart.invalidate();
		} else if (itemId == R.id.actionToggleFilled) {
			ArrayList<LineDataSet> sets = (ArrayList<LineDataSet>) mChart
					.getData().getDataSets();
			for (LineDataSet set : sets) {
				if (set.isDrawFilledEnabled())
					set.setDrawFilled(false);
				else
					set.setDrawFilled(true);
			}
			mChart.invalidate();
		} else if (itemId == R.id.actionToggleCircles) {
			ArrayList<LineDataSet> sets = (ArrayList<LineDataSet>) mChart
					.getData().getDataSets();
			for (LineDataSet set : sets) {
				if (set.isDrawCirclesEnabled())
					set.setDrawCircles(false);
				else
					set.setDrawCircles(true);
			}
			mChart.invalidate();
		} else if (itemId == R.id.actionToggleFilter) {
			// the angle of filtering is 35�
			Approximator a = new Approximator(ApproximatorType.DOUGLAS_PEUCKER,
					35);
			if (!mChart.isFilteringEnabled()) {
				mChart.enableFiltering(a);
			} else {
				mChart.disableFiltering();
			}
			mChart.invalidate();
		} else if (itemId == R.id.actionToggleStartzero) {
			mChart.getAxisLeft().setStartAtZero(
					!mChart.getAxisLeft().isStartAtZeroEnabled());
			mChart.getAxisRight().setStartAtZero(
					!mChart.getAxisRight().isStartAtZeroEnabled());
			mChart.invalidate();
		} else if (itemId == R.id.actionSave) {
			// mChart.saveToGallery("title"+System.currentTimeMillis());
			mChart.saveToPath("title" + System.currentTimeMillis(), "");
		} else if (itemId == R.id.animateX) {
			mChart.animateX(3000);
		} else if (itemId == R.id.animateY) {
			mChart.animateY(3000);
		} else if (itemId == R.id.animateXY) {
			mChart.animateXY(3000, 3000);
		}
		return true;
	}

	private int[] mColors = new int[] { ColorTemplate.VORDIPLOM_COLORS[0],
			ColorTemplate.VORDIPLOM_COLORS[1],
			ColorTemplate.VORDIPLOM_COLORS[2] };

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		Log.i("TEST", "changed");

	}

	@Override
	public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
		Log.i("VAL SELECTED",
				"Value: " + e.getVal() + ", xIndex: " + e.getXIndex()
						+ ", DataSet index: " + dataSetIndex);
	}

	@Override
	public void onNothingSelected() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {

	}

	/**
	 * Override this method to name your y coordinates
	 * 
	 * @return
	 */
	abstract protected String getYCoordinateName();

	/**
	 * Override this method to let the evaluation know which datatype should be
	 * retrieved from evaluation database
	 * 
	 * @return
	 */
	abstract protected String getDatatype();

	/**
	 * Updates the UI when new data arrives
	 */
	private void updateUI() {
		// mChart.resetTracking();

		// retrieve new eval data
		retrieveEvalData(getDatatype());
		// update new data on chart
		mChart.animateXY(3000, 3000);
		YAxis leftAxis = mChart.getAxisLeft();
		leftAxis.setAxisMinValue(0);
		leftAxis.setAxisMaxValue((float) (maxYValue * 1.2));
		Legend l = mChart.getLegend();
		l.setPosition(LegendPosition.BELOW_CHART_CENTER);
		mChart.setDescription("");
		addDataToChart();
		mChart.invalidate();

	}

	/**
	 * Adds the evaluation data to the displayed chart
	 */
	private void addDataToChart() {
		String[] mDates = new String[mKeys.size()];
		for (int j = 0; j < mKeys.size(); j++) {
			Log.e("DATE", mKeys.get(j).toString());
			mDates[j] = mKeys.get(j);

		}

		// alarmtriggers
		ArrayList<Entry> values = new ArrayList<Entry>();
		for (int j = 0; j < mKeys.size(); j++) {
			int v = mMap.get(mKeys.get(j));
			values.add(new Entry((float) v, j));
		}
		LineDataSet d = new LineDataSet(values, getYCoordinateName());
		d.setLineWidth(2.5f);
		d.setCircleSize(4f);

		int color = mColors[0];
		d.setColor(color);
		d.setCircleColor(color);
		ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();

		dataSets.add(d);
		LineData data = new LineData(mDates, dataSets);
		mChart.setData(data);

	}

	/**
	 * Retrieves evaluation data from database for each day
	 * 
	 * @param dataType
	 */
	public void retrieveEvalData(String dataType) {
		// Data
		mMap.clear();
		mKeys.clear();
		String select = EvaluationContract.C_MIMETYPE + " = ?";
		String[] selectArgs = { dataType };

		Cursor c = getContentResolver().query(
				EvaluationContract.CONTENT_URI_EVAL,
				EvaluationContract.COLUMNS_ALL, select, selectArgs, null);
		c.moveToFirst();
		long timestamp;
		long value;
		String data;
		// if cursor has entries, take first value as maximum
		if (c.getCount() > 0) {
			try {
				maxYValue = Long.parseLong(c.getString(2));
			} catch (NumberFormatException e) {
				// String is not a number, only count value up
				maxYValue = 1;
			}
		}
		while (!c.isAfterLast()) {
			timestamp = c.getLong(4);

			data = c.getString(2);
			Log.d("EVALRESULTACTIVITY", data);
			// by convention, if multiple data items are saved the last value is
			// always the data item to display
			// String[] dataArray = data
			// .split(PiaPAbstractExtension.DELIMITER_PATTERN);
			// data = dataArray[dataArray.length - 1];
			// get the number to display
			try {
				value = Long.parseLong(data);
			} catch (NumberFormatException e) {
				// String is not a number, only count value up
				value = 1;
			}
			String date = getDate(timestamp);

			int resValue = 0;
			// update stats
			resValue += value;
			// add it if there is no value for date
			if (!mKeys.contains(date)) {
				mMap.put(date, resValue);
				mKeys.add(date);
			} else {
				// add it to value for the date
				Integer oldValue = mMap.get(date);
				resValue = oldValue + resValue;

				mMap.remove(date);
				mMap.put(date, resValue);
			}
			if (resValue > maxYValue) {
				maxYValue = resValue;
			}
			c.moveToNext();
		}
		// sort the days into the correct order
		Collections.sort(mKeys);
		c.close();

	}

	/**
	 * Returns a Date initialized to the given year, month and day at midnight
	 * 
	 * @param timestamp
	 * @return
	 */
	public String getDate(long timestamp) {
		Date d;
		Calendar c = new GregorianCalendar();
		c.setTimeInMillis(timestamp);
		d = (new Date(c.getTimeInMillis()));
		SimpleDateFormat form = new SimpleDateFormat("dd-MM");
		String result = form.format(d);

		return result;
	}

}