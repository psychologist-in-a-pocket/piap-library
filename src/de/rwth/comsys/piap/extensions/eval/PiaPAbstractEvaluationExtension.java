package de.rwth.comsys.piap.extensions.eval;

import android.R;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import de.rwth.comsys.piap.contentprovider.EvaluationContract;
import de.rwth.comsys.piap.contentprovider.PluginCommunicationContract;
import de.rwth.comsys.piap.extensions.PiaPAbstractExtension;

public abstract class PiaPAbstractEvaluationExtension extends
		PiaPAbstractExtension {

	public static final String EVAL_ONGOING_KEY = "evalongoing";
	public static final String FINISHED_ACTION = "de.rwth.comsys.piap.finishedeval";
	public static final String UPDATE_UI = "de.rwth.comsys.piap.updateui";

	protected static final String PREFERENCES_EVAL = "de.rwth.comsys.piap.evalextension";
	protected static final String KEY_IS_ACTIVITY_ACTIVE = "IsActivityActive";

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("PIAPACTIVITY", "started");

		SharedPreferences prefs = getApplicationContext().getSharedPreferences(
				PREFERENCES_EVAL, Context.MODE_PRIVATE);
		prefs.edit().putBoolean(EVAL_ONGOING_KEY, false).apply();

		boolean ongoing = prefs.getBoolean(EVAL_ONGOING_KEY, false);
		// prefs.edit().putBoolean(EVAL_ONGOING_KEY, false).apply();
		if (!ongoing) {
			Log.d("PIAP", "new task");
			ComputeEvalAsyncTask task = new ComputeEvalAsyncTask(
					getApplicationContext());
			task.execute(new Void[] { null });
		}
		return Service.START_NOT_STICKY;

	}

	/**
	 * Computes the evaluation
	 */
	protected abstract void computeEval();

	/**
	 * Class to compute the evaluation in the background and start the activity
	 * to display the data at the end
	 * 
	 * @author tim
	 * 
	 */
	public class ComputeEvalAsyncTask extends AsyncTask<Void, Void, Void> {
		private final Context context;

		public ComputeEvalAsyncTask(Context context) {
			this.context = context;
		}

		@Override
		protected void onPreExecute() {
			SharedPreferences prefs = context.getSharedPreferences(
					PREFERENCES_EVAL, Context.MODE_PRIVATE);
			prefs.edit().putBoolean(EVAL_ONGOING_KEY, true).apply();
		}

		@Override
		protected Void doInBackground(Void... language) {

			computeEval();

			return null;
		}

		@Override
		protected void onPostExecute(Void nothing) {
			SharedPreferences prefs = context.getSharedPreferences(
					PREFERENCES_EVAL, Context.MODE_PRIVATE);
			boolean isActivityActive = prefs.getBoolean(KEY_IS_ACTIVITY_ACTIVE,
					true);
			if (isActivityActive) {
				Log.d("PIAPACTIVITY", "isActive");
				Intent local = new Intent();

				local.setAction(UPDATE_UI);

				context.sendBroadcast(local);
			} else {
				Log.d("PIAPACTIVITY", "isNotActive");

				// show notifaction result is available
				Notification.Builder mBuilder = new Notification.Builder(
						context).setContentTitle(getName() + " Finished!")
						.setContentText("The evaluation has finished")
						.setSmallIcon(R.drawable.ic_menu_call)
						.setAutoCancel(true);
				Intent resultIntent = new Intent(context, getActivityName());
				// Because clicking the notification opens a new ("special")
				// activity, there's
				// no need to create an artificial back stack.
				PendingIntent resultPendingIntent = PendingIntent.getActivity(
						context, 0, resultIntent,
						PendingIntent.FLAG_UPDATE_CURRENT);
				mBuilder.setContentIntent(resultPendingIntent);
				int mNotificationId = 001;
				// Gets an instance of the NotificationManager service
				NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
				// Builds the notification and issues it.
				mNotifyMgr.notify(mNotificationId, mBuilder.build());
			}
			Log.d("TESTE", "ONGOING KEY FALSE");
			prefs.edit().putBoolean(EVAL_ONGOING_KEY, false).apply();
			sendEvalFinishedIntent();

		}

	}

	/**
	 * Broadcasts that the service has finished computing the evaluation
	 */
	protected void sendEvalFinishedIntent() {
		Intent intent = new Intent(FINISHED_ACTION);
		sendBroadcast(intent);
	}

	abstract protected Class<?> getActivityName();

	private String getName() {
		return getClass().getSimpleName();
	}

	/**
	 * Create a database entry of the evaluation data
	 * 
	 * @param data
	 *            The raw data item
	 * @param timestamp
	 *            The timestamp belonging to the data item
	 * @param score
	 *            The score the data item received
	 */
	protected void addEvalDataToDB(String data, long timestamp, long score,
			String datatype) {
		Log.d("INSERT", "Data: " + data + " Timestamp: " + timestamp
				+ " Score: " + score + " Datatype: " + datatype);
		ContentValues values = new ContentValues();
		values.put(EvaluationContract.C_MIMETYPE, datatype);

		values.put(EvaluationContract.C_DATA, data);
		// Log.d(TAG, "Plugin ID" + getPackageName());

		values.put(EvaluationContract.C_PLUGINID, getClass().getCanonicalName());
		// Date date = new Date();
		values.put(EvaluationContract.C_TIMESTAMP, timestamp);
		values.put(EvaluationContract.C_SCORE, score);

		Uri dataUri = getContentResolver().insert(
				EvaluationContract.CONTENT_URI_EVAL, values);
	}

	protected void deleteOldEvalData(String dataType) {
		String where = EvaluationContract.C_MIMETYPE + " = ?";
		String[] args = { dataType };

		getContentResolver().delete(EvaluationContract.CONTENT_URI_EVAL, where,
				args);

	}

	protected void deleteDataEntry(String data, long timestamp) {
		String where = PluginCommunicationContract.C_DATA1 + " = ? AND "
				+ PluginCommunicationContract.C_TIMESTAMP + " = ?";
		String[] args = { data, "" + timestamp };

		getContentResolver()
				.delete(PluginCommunicationContract.CONTENT_URI_PLUGINDATA,
						where, args);

	}

	/**
	 * Class to save the evaluation result
	 * 
	 * @author tim
	 * 
	 */
	public class EvalNumericData {
		private long timestamp;
		private long data;

		public EvalNumericData(long timestamp, long data) {
			this.data = data;
			this.timestamp = timestamp;
		}

		public long getTimestamp() {
			return timestamp;
		}

		public void setTimestamp(long timestamp) {
			this.timestamp = timestamp;
		}

		public long getData() {
			return data;
		}

		public void setData(long data) {
			this.data = data;
		}
	}
}
