package de.rwth.comsys.piap.extensions.data;

import java.util.Date;

import android.content.ContentValues;
import android.net.Uri;
import de.rwth.comsys.piap.contentprovider.PluginCommunicationContract;
import de.rwth.comsys.piap.extensions.PiaPAbstractExtension;

public abstract class PiaPGatherDataExtension extends PiaPAbstractExtension {

	/**
	 * The String representing a Piap Extension. Check for it with Intent
	 * filter.
	 */
	public static final String ACTION_PLUGIN = "de.rwth.comsys.piap.Plugin";

	public PiaPGatherDataExtension() {

	}

	protected void addSensorDataToDB(String data, String dataType) {
		ContentValues values = new ContentValues();
		values.put(PluginCommunicationContract.C_MIMETYPE, dataType);
		values.put(PluginCommunicationContract.C_DATA1, data);
		// Log.d(TAG, "Plugin ID" + getPackageName());

		values.put(PluginCommunicationContract.C_PLUGINID, getClass()
				.getCanonicalName());
		Date date = new Date();
		values.put(PluginCommunicationContract.C_TIMESTAMP, date.getTime());
		values.put(PluginCommunicationContract.C_DEVICE,
				PluginCommunicationContract.BLUETOOTH_ORIGINAL_DATA);

		Uri dataUri = getContentResolver().insert(
				PluginCommunicationContract.CONTENT_URI_PLUGINDATA, values);
	}

}
