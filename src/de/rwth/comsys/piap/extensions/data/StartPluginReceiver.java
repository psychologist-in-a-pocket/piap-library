package de.rwth.comsys.piap.extensions.data;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import de.rwth.comsys.piap.extensions.PiaPAbstractReceiver;

public abstract class StartPluginReceiver extends PiaPAbstractReceiver {

	private final String LOCK_KEY = "lock";
	public Context context;

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d("Receiver", "alarm manager created");
		this.context = context;
		SharedPreferences prefs = context.getSharedPreferences(
				getPreferences(), Context.MODE_PRIVATE);
		boolean lock = prefs.getBoolean(LOCK_KEY, false);
		String action = intent.getAction();
		Intent myIntent = new Intent(context, getServiceName());
		Intent myActivity;

		if (lock) {
			if (action.equals(getAction() + ".START")) {
				Log.d("REC", "START SERVICE, REMOVE LOCK");

				prefs.edit().putBoolean(LOCK_KEY, false).apply();
				if (getActivityName() != null) {
					myActivity = new Intent(context, getActivityName());
					myActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(myActivity);
				}

				context.startService(myIntent);
			} else {
				Log.d("REC", "Wait for lock release first");
				// do nothing, wait for lock release
			}
		} else {

			if (action.equals(getAction() + ".STOP")) {
				Log.d("REC", "STOP SERVICE AND LOCK");
				prefs.edit().putBoolean(LOCK_KEY, true).apply();
				context.stopService(myIntent);
			} else if (action.equals("android.intent.action.BOOT_COMPLETED")) {
				Log.d("Receiver", "boot completed");
				context.startService(myIntent);
				if (getActivityName() != null) {
					myActivity = new Intent(context, getActivityName());
					myActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(myActivity);
				}
			} else if (action.equals(getCustomAction())) {
				getCustomBehaviour();
			}
		}
	}

	protected void getCustomBehaviour() {
	}

	protected String getCustomAction() {
		return "";
	}

}
