package de.rwth.comsys.piap.extensions;

import android.content.BroadcastReceiver;

public abstract class PiaPAbstractReceiver extends BroadcastReceiver {

	abstract protected String getAction();

	protected Class<?> getActivityName() {
		return null;
	}

	abstract protected Class<?> getServiceName();

	abstract protected String getPreferences();

}
