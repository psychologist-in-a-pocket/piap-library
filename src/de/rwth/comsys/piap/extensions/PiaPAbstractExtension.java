package de.rwth.comsys.piap.extensions;

import android.app.Service;

public abstract class PiaPAbstractExtension extends Service {

	public static final int DATA_COLUMN = 2;
	public static final int TIMESTAMP_COLUMN = 4;
	public static final long DEPRESSION_STATE_WARNING = 1;
	public static final long DEPRESSION_STATE_OKAY = 0;
	public static final long DAY_IN_MS = 1000 * 60 * 60 * 24;
	public static final String DELIMITER = "   ";
	public static final String DELIMITER_PATTERN = "   ";

}
