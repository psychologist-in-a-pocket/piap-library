package de.rwth.comsys.piap.ipc;

import com.google.protobuf.MessageOrBuilder;

public interface IProtoMessageReceiver<E extends MessageOrBuilder> {
	public void onReceive(E mob);
}
