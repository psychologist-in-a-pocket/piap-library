package de.rwth.comsys.piap.ipc;

public interface IProtoOnConnect {
	public void onConnect();
}
