package de.rwth.comsys.piap.ipc;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.net.LocalSocket;

import com.google.protobuf.MessageOrBuilder;

public abstract class ProtoSocket<E extends MessageOrBuilder> {

	protected InputStream inStream;
	protected OutputStream outStream;
	protected LocalSocket localSocket;

	private IProtoMessageReceiver<E> messageReceiveCallback;
	private IProtoMessageParser<E> messageParserCallback;

	boolean parserThreadRunning = false;
	boolean connectOnThreadRunning = false;

	public ProtoSocket(IProtoMessageReceiver<E> messageReceiveCallback, IProtoMessageParser<E> messageParserCallback) {
		super();

		if (messageParserCallback == null) {
			throw new RuntimeException("messageParserCallback can not be null");
		}

		if (messageReceiveCallback == null) {
			throw new RuntimeException("messageReceiveCallback can not be null");
		}

		this.messageReceiveCallback = messageReceiveCallback;
		this.messageParserCallback = messageParserCallback;
	}

	public InputStream getInputStream() {
		return inStream;
	}

	public OutputStream getOutPutStream() {
		return outStream;
	}

	public void connect() {
		connectOnThreadRunning = true;
		threadConnect.start();
	}

	protected abstract void connectOnThread();

	protected abstract void closeImpl();

	public void close() {
		// let derived class close itself
		closeImpl();
		// then finish our member stuff
		threadConnect.interrupt();
		// maybe someoen forgto to stop this:
		stopPollingInStreamForMessages();
	}

	protected void startPollingInStreamForMessages() {
		parserThreadRunning = true;
		threadPoll.start();
	}

	protected void stopPollingInStreamForMessages() {
		parserThreadRunning = false;
		threadPoll.interrupt();
	}

	Runnable connectOnThread = new Runnable() {

		@Override
		public void run() {
			connectOnThread();
		}
	};

	Runnable pollProtoSocketThread = new Runnable() {

		@Override
		public void run() {
			while (parserThreadRunning && localSocket != null && localSocket.isConnected()) {

				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					E mob = messageParserCallback.parseMessageFromStream(inStream);
					messageReceiveCallback.onReceive(mob);
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}

		}
	};

	Thread threadPoll = new Thread(pollProtoSocketThread);
	Thread threadConnect = new Thread(connectOnThread);
}