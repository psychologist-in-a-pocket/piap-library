package de.rwth.comsys.piap.ipc;

import java.io.IOException;

import android.net.LocalSocket;
import android.net.LocalSocketAddress;

import com.google.protobuf.MessageOrBuilder;

public class ProtoIPCClient<E extends MessageOrBuilder> extends ProtoSocket<E> {
	private String socketName;

	public ProtoIPCClient(String socketName, IProtoMessageReceiver<E> messageReceiveCallback, IProtoMessageParser<E> messageParserCallback) {
		super(messageReceiveCallback, messageParserCallback);
		this.socketName = socketName;
	}

	@Override
	protected void connectOnThread() {
		localSocket = new LocalSocket();
		try {
			localSocket.connect(new LocalSocketAddress(socketName));
			inStream = localSocket.getInputStream();
			outStream = localSocket.getOutputStream();
			startPollingInStreamForMessages();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void closeImpl() {
		stopPollingInStreamForMessages();
		if (inStream != null) {
			try {
				inStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (outStream != null) {
			try {
				outStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (localSocket != null) {
			try {
				localSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
