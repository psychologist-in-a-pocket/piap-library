package de.rwth.comsys.piap.ipc;

import java.io.IOException;

import android.net.LocalServerSocket;

import com.google.protobuf.MessageOrBuilder;

public class ProtoIPCServer<E extends MessageOrBuilder> extends ProtoSocket<E> {
	private LocalServerSocket server;

	private IProtoOnConnect onConnectCallback;

	public ProtoIPCServer(String socketName, IProtoMessageReceiver<E> messageReceiveCallback, IProtoMessageParser<E> messageParserCallback,
			IProtoOnConnect onConnectCallback) {
		super(messageReceiveCallback, messageParserCallback);

		if (onConnectCallback == null) {
			throw new RuntimeException("onConnectCallback can not be null");
		}
		this.onConnectCallback = onConnectCallback;

		try {
			server = new LocalServerSocket(socketName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void connectOnThread() {
		while (connectOnThreadRunning) {
			try {
				localSocket = server.accept();
				if (localSocket != null) {
					try {
						inStream = localSocket.getInputStream();
						outStream = localSocket.getOutputStream();
						startPollingInStreamForMessages();
					} catch (IOException e) {
						e.printStackTrace();
					}

				}
				onConnectCallback.onConnect();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

	@Override
	public void closeImpl() {
		connectOnThreadRunning = false;
		stopPollingInStreamForMessages();

		if (inStream != null) {
			try {
				inStream.close();
			} catch (Exception e) {
				e.printStackTrace();

			}
		}

		if (outStream != null) {
			try {
				outStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (localSocket != null) {
			try {
				localSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
}
