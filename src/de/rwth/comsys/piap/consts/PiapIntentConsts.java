package de.rwth.comsys.piap.consts;

public class PiapIntentConsts {
	public static final String INTENT_EXTRA_SOCKET_NAME = "socketName";
	public static final String INTENT_ACTION_RECEIVE_PIAP_LOGS = "de.rwth.comsys.piap.RECIEVE_LOGS";
}
