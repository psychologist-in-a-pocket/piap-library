package de.rwth.comsys.piap.contentprovider;

import android.net.Uri;

public final class PluginCommunicationContract {
	private PluginCommunicationContract() {
	}

	/** Authority string for this provider. */
	public static final String AUTHORITY = "de.rwth.comsys.piap.plugincommunicationprovider";
	public static final String DATATYPE_LOGS = "application/vnd.tim.logging";
	public static final String PLUGIN_PATH = "PluginDataTable";

	public static final int PLUGINTABLE = 1;
	public static final int PLUGINTABLE_ID = 2;
	public static final String BLUETOOTH_ORIGINAL_DATA = "origin";
	public static final String BLUETOOTH_FOREIGN_DATA = "other";

	/**
	 * The content:// style URL for this provider
	 */
	public static final Uri CONTENT_URI_PLUGINDATA = Uri.parse("content://"
			+ AUTHORITY + "/" + PLUGIN_PATH);
	/**
	 * The MIME type of the results from {@link #CONTENT_URI_PLUGINDATA}.
	 */
	public static final String CONTENT_TYPE_PLUGINDATA = "vnd.android.cursor.dir/vnd.plugin.logs";
	public static final String CONTENT_ITEM_TYPE_PLUGINDATA = "vnd.android.cursor.item/vnd.plugin.log";

	// Columns
	public static final String C_ID = "_id";
	public static final String C_MIMETYPE = "mimetype";
	/** Generic data column, the meaning is {@link #C_MIMETYPE} specific */
	public static final String C_DATA1 = "data1";
	public static final String C_PLUGINID = "appid";
	public static final String C_TIMESTAMP = "timestamp";
	public static final String C_DEVICE = "device";

	public static final String[] COLUMNS_ALL = { C_ID, C_MIMETYPE, C_DATA1,
			C_PLUGINID, C_TIMESTAMP, C_DEVICE };

}
