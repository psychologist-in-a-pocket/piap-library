package de.rwth.comsys.piap.contentprovider;

import android.net.Uri;

public final class EvaluationContract {
	private EvaluationContract() {
	}

	/** Authority string for this provider. */
	public static final String AUTHORITY = "de.rwth.comsys.piap.evaluationprovider";

	public static final String EVAL_PATH = "EvaluationTable";

	public static final int EVALTABLE = 3;
	public static final int EVALTABLE_ID = 4;

	/**
	 * The content:// style URL for this provider
	 */
	public static final Uri CONTENT_URI_EVAL = Uri.parse("content://"
			+ AUTHORITY + "/" + EVAL_PATH);
	/**
	 * The MIME type of the results from {@link #CONTENT_URI_PLUGINDATA}.
	 */
	public static final String CONTENT_TYPE_EVAL = "vnd.android.cursor.dir/vnd.plugin.evaluation";
	public static final String CONTENT_ITEM_TYPE_EVAL = "vnd.android.cursor.item/vnd.plugin.evaluationRow";

	// Columns
	public static final String C_ID = "_id";
	public static final String C_DATA = "data";
	public static final String C_SCORE = "score";

	public static final String C_PLUGINID = "appid";
	public static final String C_TIMESTAMP = "timestamp";
	public static final String C_MIMETYPE = "mimetype";
	/** Generic data column, the meaning is {@link #C_MIMETYPE} specific */
	public static final String[] COLUMNS_ALL = { C_ID, C_MIMETYPE, C_DATA,
			C_PLUGINID, C_TIMESTAMP, C_SCORE };

}
