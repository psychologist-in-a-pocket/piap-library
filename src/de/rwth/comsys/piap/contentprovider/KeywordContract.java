package de.rwth.comsys.piap.contentprovider;

import android.net.Uri;

public final class KeywordContract {
	private KeywordContract() {
	}

	/** Authority string for this provider. */
	public static final String AUTHORITY = "de.rwth.comsys.piap.keywordprovider";

	public static final String KEYWORD_PATH = "KeywordTable";

	public static final int KEYWORDTABLE = 5;
	public static final int KEYWORDTABLE_ID = 6;

	/**
	 * The content:// style URL for this provider
	 */
	public static final Uri CONTENT_URI_KEYWORD = Uri.parse("content://"
			+ AUTHORITY + "/" + KEYWORD_PATH);
	/**
	 * The MIME type of the results from {@link #CONTENT_URI_PLUGINDATA}.
	 */
	public static final String CONTENT_TYPE_KEYWORD = "vnd.android.cursor.dir/vnd.plugin.keyword";
	public static final String CONTENT_ITEM_TYPE_KEYWORD = "vnd.android.cursor.item/vnd.plugin.keywordRow";

	// Columns
	public static final String C_ID = "_id";
	public static final String C1_KEYWORD = "name"; // Keyword
	public static final String C2_CATEGORY = "category"; // Category
	public static final String C3_INFO = "info";
	/** Generic data column, the meaning is {@link #C_MIMETYPE} specific */
	public static final String[] COLUMNS_ALL = { C_ID, C1_KEYWORD, C2_CATEGORY,
			C3_INFO };

}
