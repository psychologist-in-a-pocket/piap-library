package de.rwth.comsys.piap.contentprovider;

import android.net.Uri;

public final class ClinicalRatingContract {
	private ClinicalRatingContract() {
	}

	/** Authority string for this provider. */
	public static final String AUTHORITY = "de.rwth.comsys.piap.clinicalratingprovider";

	public static final String BASE_PATH = "RatingTable";
	public static final int SIMPLE_RATING = 1;
	public static final int RATING_WITH_PREPROCESSING = 2;
	public static final int RESULT_PROBABLY_CORRECTION_FOR_A_TYPO = 1;
	public static final int RESULT_ORIGINAL_WORD = 0;
	/**
	 * The content:// style URL for this provider
	 */
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + BASE_PATH);

	/**
	 * A query parameter that limits the number of results returned. The
	 * parameter value should be an integer.
	 */
	public static final String LIMIT_PARAM_KEY = "limit";

	/**
	 * This enables a content provider to remove duplicate entries in results.
	 * 
	 * @hide
	 */
	public static final String REMOVE_DUPLICATE_ENTRIES = "remove_duplicate_entries";
}
