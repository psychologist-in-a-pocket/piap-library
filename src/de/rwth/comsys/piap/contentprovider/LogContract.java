package de.rwth.comsys.piap.contentprovider;

import android.net.Uri;

public final class LogContract {
	private LogContract() {
	}

	/** Authority string for this provider. */
	public static final String AUTHORITY = "de.rwth.comsys.piap.logprovider";

	public static final String LOG_PATH = "LogTable";

	public static final int LOGTABLE = 7;
	public static final int LOGTABLE_ID = 8;

	/**
	 * The content:// style URL for this provider
	 */
	public static final Uri CONTENT_URI_LOG = Uri.parse("content://"
			+ AUTHORITY + "/" + LOG_PATH);
	/**
	 * The MIME type of the results from {@link #CONTENT_URI_PLUGINDATA}.
	 */
	public static final String CONTENT_TYPE_LOG = "vnd.android.cursor.dir/vnd.plugin.log";
	public static final String CONTENT_ITEM_TYPE_LOG = "vnd.android.cursor.item/vnd.plugin.logRow";

	// Columns
	public static final String C0_TIMESTAMP = "_id";
	public static final String C1_APP_NAME = "app_name";
	public static final String C2_APP_PKG = "app_pkg";
	public static final String C3_INPUT = "input";
	/** Generic data column, the meaning is {@link #C_MIMETYPE} specific */
	public static final String[] COLUMNS_ALL = { C0_TIMESTAMP, C1_APP_NAME,
			C2_APP_PKG, C3_INPUT };

}
